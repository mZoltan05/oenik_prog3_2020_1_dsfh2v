/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ingatlan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Ingatlan {
    
    static final long serialVersionUID = 1L;
    
    @XmlElement
    int ker;
    @XmlElement
    int butorozott;
    @XmlElement
    int berleti_dij;
    @XmlElement
    int negyzetmeter;
    @XmlElement
    int tipus_id;

    public Ingatlan() {
    }

    public Ingatlan(int ker, int butorozott, int berleti_dij, int negyzetmeter, int tipus_id) {
        this.ker = ker;
        this.butorozott = butorozott;
        this.berleti_dij = berleti_dij;
        this.negyzetmeter = negyzetmeter;
        this.tipus_id = tipus_id;
    }

    public int getKer() {
        return ker;
    }

    public int getButorozott() {
        return butorozott;
    }

    public int getBerleti_dij() {
        return berleti_dij;
    }

    public int getNegyzetmeter() {
        return negyzetmeter;
    }

    public int getTipus_id() {
        return tipus_id;
    }

    
    
}
