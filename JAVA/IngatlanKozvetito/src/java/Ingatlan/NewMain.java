/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ingatlan;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;


public class NewMain {

    public static void main(String[] args) throws JAXBException {
        Filter filt = new Filter();
        filt.setFilterTipus(1);
        
        List<Ingatlan> ingatlanok = Repository.getInstance().getIngatlanok();
        
        ListIngatlan ing = new ListIngatlan(filt.Filter(ingatlanok));    
        
        
            JAXBContext ctx = JAXBContext.newInstance(ListIngatlan.class, ArrayList.class);
            Marshaller marschaller = ctx.createMarshaller();
            marschaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            marschaller.marshal(ing, sw);
            System.out.println(sw.toString());
            System.out.println("hello");
    }
    
}
