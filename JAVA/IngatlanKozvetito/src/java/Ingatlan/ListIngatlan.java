/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ingatlan;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ListIngatlan {
    @XmlElement
    List<Ingatlan> ingatlanok;

    public ListIngatlan(List<Ingatlan> ingatlanok) {
        this.ingatlanok = ingatlanok;
    }

    public List<Ingatlan> getIngatlanok() {
        return ingatlanok;
    }
    
    

    public ListIngatlan() {
    }
    
}
