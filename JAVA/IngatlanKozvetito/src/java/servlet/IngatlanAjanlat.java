
package servlet;

import Ingatlan.Ingatlan;
import Ingatlan.ListIngatlan;
import Ingatlan.Filter;
import Ingatlan.Repository;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@WebServlet(name = "IngatlanAjanlat", urlPatterns = {"/IngatlanAjanlat"})
public class IngatlanAjanlat extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        
        Filter filter = new Filter();
        if (request.getParameter("tipus") != null && request.getParameter("tipus").trim() != ""){
            int tipus = Integer.parseInt(request.getParameter("tipus"));
            filter.setFilterTipus(tipus);
        }
        if (request.getParameter("ar") != null && request.getParameter("ar").trim() != ""){
            int ar = Integer.parseInt(request.getParameter("ar"));
            filter.setFilterBerleti_dj(ar);
        }
       
        
        
        
        List<Ingatlan> ingatlanok = Repository.getInstance().getIngatlanok();
        ListIngatlan ing = new ListIngatlan(filter.Filter(ingatlanok));

        

        try {
            JAXBContext ctx = JAXBContext.newInstance(ListIngatlan.class, ArrayList.class);
            Marshaller marschaller = ctx.createMarshaller();
            marschaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marschaller.marshal(ing, response.getOutputStream());
        } catch (JAXBException e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
