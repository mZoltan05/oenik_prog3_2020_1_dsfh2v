var hierarchy =
[
    [ "IngatlanKozvetito.Data.berlesek", "class_ingatlan_kozvetito_1_1_data_1_1berlesek.html", null ],
    [ "DbContext", null, [
      [ "IngatlanKozvetito.Data.IngatlanDBEntities", "class_ingatlan_kozvetito_1_1_data_1_1_ingatlan_d_b_entities.html", null ]
    ] ],
    [ "IngatlanKozvetito.Logic.ILogic", "interface_ingatlan_kozvetito_1_1_logic_1_1_i_logic.html", [
      [ "IngatlanKozvetito.Logic.BusinessLogic", "class_ingatlan_kozvetito_1_1_logic_1_1_business_logic.html", null ]
    ] ],
    [ "IngatlanKozvetito.Data.ingatlanok", "class_ingatlan_kozvetito_1_1_data_1_1ingatlanok.html", null ],
    [ "IngatlanKozvetito.Logic.IngatlanTipusAtlagar", "class_ingatlan_kozvetito_1_1_logic_1_1_ingatlan_tipus_atlagar.html", null ],
    [ "IngatlanKozvetito.Repository.IRepository< T >", "interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html", [
      [ "IngatlanKozvetito.Repository.Repository< T >", "class_ingatlan_kozvetito_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "IngatlanKozvetito.Repository.IRepository< IngatlanKozvetito.Data.berlesek >", "interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html", null ],
    [ "IngatlanKozvetito.Repository.IRepository< IngatlanKozvetito.Data.ingatlanok >", "interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html", null ],
    [ "IngatlanKozvetito.Repository.IRepository< IngatlanKozvetito.Data.menedzserek >", "interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html", null ],
    [ "IngatlanKozvetito.Repository.IRepository< IngatlanKozvetito.Data.tipusok >", "interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html", null ],
    [ "IngatlanKozvetito.Repository.IRepository< IngatlanKozvetito.Data.ugyfelek >", "interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html", null ],
    [ "IngatlanKozvetito.Data.menedzserek", "class_ingatlan_kozvetito_1_1_data_1_1menedzserek.html", null ],
    [ "IngatlanKozvetito.Logic.Tests.Test", "class_ingatlan_kozvetito_1_1_logic_1_1_tests_1_1_test.html", null ],
    [ "IngatlanKozvetito.Data.tipusok", "class_ingatlan_kozvetito_1_1_data_1_1tipusok.html", null ],
    [ "IngatlanKozvetito.Data.ugyfelek", "class_ingatlan_kozvetito_1_1_data_1_1ugyfelek.html", null ]
];