var searchData=
[
  ['data_20',['Data',['../namespace_ingatlan_kozvetito_1_1_data.html',1,'IngatlanKozvetito']]],
  ['ilogic_21',['ILogic',['../interface_ingatlan_kozvetito_1_1_logic_1_1_i_logic.html',1,'IngatlanKozvetito::Logic']]],
  ['ingatlandbentities_22',['IngatlanDBEntities',['../class_ingatlan_kozvetito_1_1_data_1_1_ingatlan_d_b_entities.html',1,'IngatlanKozvetito::Data']]],
  ['ingatlankozvetito_23',['IngatlanKozvetito',['../namespace_ingatlan_kozvetito.html',1,'']]],
  ['ingatlanok_24',['ingatlanok',['../class_ingatlan_kozvetito_1_1_data_1_1ingatlanok.html',1,'IngatlanKozvetito::Data']]],
  ['ingatlantipusatlagar_25',['IngatlanTipusAtlagar',['../class_ingatlan_kozvetito_1_1_logic_1_1_ingatlan_tipus_atlagar.html',1,'IngatlanKozvetito::Logic']]],
  ['irepository_26',['IRepository',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3aberlesek_20_3e_27',['IRepository&lt; IngatlanKozvetito::Data::berlesek &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3aingatlanok_20_3e_28',['IRepository&lt; IngatlanKozvetito::Data::ingatlanok &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3amenedzserek_20_3e_29',['IRepository&lt; IngatlanKozvetito::Data::menedzserek &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3atipusok_20_3e_30',['IRepository&lt; IngatlanKozvetito::Data::tipusok &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['irepository_3c_20ingatlankozvetito_3a_3adata_3a_3augyfelek_20_3e_31',['IRepository&lt; IngatlanKozvetito::Data::ugyfelek &gt;',['../interface_ingatlan_kozvetito_1_1_repository_1_1_i_repository.html',1,'IngatlanKozvetito::Repository']]],
  ['logic_32',['Logic',['../namespace_ingatlan_kozvetito_1_1_logic.html',1,'IngatlanKozvetito']]],
  ['program_33',['Program',['../namespace_ingatlan_kozvetito_1_1_program.html',1,'IngatlanKozvetito']]],
  ['repository_34',['Repository',['../namespace_ingatlan_kozvetito_1_1_repository.html',1,'IngatlanKozvetito']]],
  ['tests_35',['Tests',['../namespace_ingatlan_kozvetito_1_1_logic_1_1_tests.html',1,'IngatlanKozvetito::Logic']]]
];
