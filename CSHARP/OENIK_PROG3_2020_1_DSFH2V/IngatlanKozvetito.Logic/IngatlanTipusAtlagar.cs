﻿// <copyright file="IngatlanTipusAtlagar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class is necessary for AtlagIngatlanar method.
    /// </summary>
    public class IngatlanTipusAtlagar
    {
        /// <summary>
        /// Gets or sets type of real estate.
        /// </summary>
       public string Tipus { get; set; }

        /// <summary>
        /// Gets or sets average price of real estate.
        /// </summary>
       public int Atlagar { get; set; }
    }
}
