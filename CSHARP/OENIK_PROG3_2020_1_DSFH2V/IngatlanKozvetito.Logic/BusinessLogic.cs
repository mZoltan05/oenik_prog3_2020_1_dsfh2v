﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using IngatlanKozvetito.Data;
    using IngatlanKozvetito.Repository;

    /// <summary>
    /// Business Logic.
    /// </summary>
    public class BusinessLogic : ILogic
    {
        private readonly IRepository<menedzserek> menedzserek;
        private readonly IRepository<berlesek> berlesek;
        private readonly IRepository<tipusok> tipusok;
        private readonly IRepository<ingatlanok> ingatlanok;
        private readonly IRepository<ugyfelek> ugyfelek;

    /// <summary>
    /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
    /// </summary>
        public BusinessLogic()
        {
            this.menedzserek = new Repository<menedzserek>();
            this.berlesek = new Repository<berlesek>();
            this.tipusok = new Repository<tipusok>();
            this.ingatlanok = new Repository<ingatlanok>();
            this.ugyfelek = new Repository<ugyfelek>();
        }

    /// <summary>
    /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
    /// </summary>
    /// <param name="menedzserr">Menedzser entity.</param>
    /// <param name="berlesr">Berles entity.</param>
    /// <param name="tipusr">Tipus entity.</param>
    /// <param name="ingatlanr">Ingatlan entity.</param>
    /// <param name="ugyfelr">Ugyfel entity.</param>
        public BusinessLogic(IRepository<menedzserek> menedzserr, IRepository<berlesek> berlesr, IRepository<tipusok> tipusr, IRepository<ingatlanok> ingatlanr, IRepository<ugyfelek> ugyfelr)
        {
            this.menedzserek = menedzserr;
            this.berlesek = berlesr;
            this.tipusok = tipusr;
            this.ingatlanok = ingatlanr;
            this.ugyfelek = ugyfelr;
        }

        /// <inheritdoc/>
        public void CreateBerles(berlesek uj)
        {
            this.berlesek.Create(uj);
        }

        /// <inheritdoc/>
        public void CreateIngatlan(ingatlanok uj)
        {
            this.ingatlanok.Create(uj);
        }

        /// <inheritdoc/>
        public void CreateMenedzser(menedzserek uj)
        {
            this.menedzserek.Create(uj);
        }

        /// <inheritdoc/>
        public void CreateTipus(tipusok uj)
        {
            this.tipusok.Create(uj);
        }

        /// <inheritdoc/>
        public void CreateUgyfel(ugyfelek uj)
        {
            this.ugyfelek.Create(uj);
        }

        /// <inheritdoc/>
        public void DeleteBerles(string id)
        {
            this.berlesek.Delete(id);
        }

        /// <inheritdoc/>
        public void DeleteIngatlan(string id)
        {
            this.ingatlanok.Delete(id);
        }

        /// <inheritdoc/>
        public void DeleteMenedzser(string id)
        {
            this.menedzserek.Delete(id);
        }

        /// <inheritdoc/>
        public void DeleteTipus(string id)
        {
            this.tipusok.Delete(id);
        }

        /// <inheritdoc/>
        public void DeleteUgyfel(string id)
        {
            this.ugyfelek.Delete(id);
        }

        /// <inheritdoc/>
        public IQueryable<berlesek> ReadBerles()
        {
            return this.berlesek.Read();
        }

        /// <inheritdoc/>
        public IQueryable<ingatlanok> ReadIngatlan()
        {
            return this.ingatlanok.Read();
        }

        /// <inheritdoc/>
        public IQueryable<menedzserek> ReadMenedzser()
        {
            return this.menedzserek.Read();
        }

        /// <inheritdoc/>
        public IQueryable<tipusok> ReadTipus()
        {
            return this.tipusok.Read();
        }

        /// <inheritdoc/>
        public IQueryable<ugyfelek> ReadUgyfel()
        {
            return this.ugyfelek.Read();
        }

        /// <inheritdoc/>
        public void UpdateBerles(string id, string attributeName, string newValue)
        {
            this.berlesek.Update(id, attributeName, newValue);
        }

        /// <inheritdoc/>
        public void UpdateIngatlan(string id, string attributeName, string newValue)
        {
            this.ingatlanok.Update(id, attributeName, newValue);
        }

        /// <inheritdoc/>
        public void UpdateMenedzser(string id, string attributeName, string newValue)
        {
            this.menedzserek.Update(id, attributeName, newValue);
        }

        /// <inheritdoc/>
        public void UpdateTipus(string id, string attributeName, string newValue)
        {
            this.tipusok.Update(id, attributeName, newValue);
        }

        /// <inheritdoc/>
        public void UpdateUgyfel(string id, string attributeName, string newValue)
        {
            this.ugyfelek.Update(id, attributeName, newValue);
        }

        /// <inheritdoc/>
        public List<IngatlanTipusAtlagar> AtlagIngatlanar()
        {
            List<IngatlanTipusAtlagar> lista = new List<IngatlanTipusAtlagar>();
            var ingatlanok = from b in this.ReadIngatlan().ToList()
                             join c in this.ReadTipus().ToList() on b.tipus_id equals c.tipus_id
                             group b by c.tipus_nev into tipus
                             select new
                             {
                                 Tipus = tipus.Key, Atlag = tipus.Average(x => x.berleti_dij),
                             };
            foreach (var item in ingatlanok)
            {
                lista.Add(new IngatlanTipusAtlagar() { Tipus = item.Tipus, Atlagar = Convert.ToInt32(Math.Round(Convert.ToDouble(item.Atlag))) });
            }

            return lista;
        }

        /// <inheritdoc/>
        public List<ingatlanok> HaromLegolcsobbKereses(int ker)
        {
            List<ingatlanok> lista = new List<ingatlanok>();
            var ingatlanok = from x in this.ReadIngatlan().ToList()
                             join y in this.ReadTipus().ToList() on x.tipus_id equals y.tipus_id
                             where x.ker == ker && y.lakhato == 1
                             orderby x.berleti_dij
                             select x;
            int i = 0;
            foreach (ingatlanok item in ingatlanok)
            {
                if (i++ < 3)
                {
                    lista.Add(item);
                }
            }

            return lista;
        }

        /// <inheritdoc/>
        public List<string> UgyfelekBerletiSzerint()
        {
            List<string> lista = new List<string>();
            var ugyfelek = from x in this.ReadUgyfel().ToList()
                           join y in this.ReadBerles().ToList() on x.berlo_id equals y.berlo_id
                           join z in this.ReadIngatlan().ToList() on y.ingatlan_id equals z.ingatlan_id
                           orderby z.berleti_dij descending
                           select new
                           {
                               Nev = x.nev,
                               Dij = z.berleti_dij,
                           };
            string ugyfel;
            foreach (var item in ugyfelek)
            {
                ugyfel = item.Nev + " - " + item.Dij + "Ft";
                lista.Add(ugyfel);
            }

            return lista;
        }

        /// <inheritdoc/>
        public List<ingatlanok> AjanlatKeres(int tipus_id, int berleti_dij)
        {
            List<ingatlanok> lista = new List<ingatlanok>();
            string url = $"http://localhost:8080/IngatlanKozvetito/IngatlanAjanlat?tipus={tipus_id}&ar={berleti_dij}";
            XDocument doc = XDocument.Load(url);
            foreach (var item in doc.Root.Descendants("ingatlanok"))
            {
                lista.Add(new ingatlanok
                {
                    ker = int.Parse(item.Element("ker").Value), butorozott = int.Parse(item.Element("butorozott").Value),
                    berleti_dij = int.Parse(item.Element("berleti_dij").Value),
                    negyzetmeter = int.Parse(item.Element("negyzetmeter").Value),
                    tipus_id = int.Parse(item.Element("tipus_id").Value),
                });
            }

            return lista;
        }
    }
}
