﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository interface.
    /// </summary>
    /// <typeparam name="T">Generic Type.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Create method.
        /// </summary>
        /// <param name="tartalom">Content.</param>
        void Create(T tartalom);

        /// <summary>
        /// Delete method.
        /// </summary>
        /// <param name="id">Object id.</param>
        void Delete(string id);

        /// <summary>
        /// Read method.
        /// </summary>
        /// <returns>T type.</returns>
        IQueryable<T> Read();

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="id">Object id.</param>
        /// <param name="attributeName">Attribute name.</param>
        /// <param name="newValue">New value.</param>
        void Update(string id, string attributeName, string newValue);
    }
}
