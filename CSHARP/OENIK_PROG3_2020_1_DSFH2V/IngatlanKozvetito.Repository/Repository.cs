﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Repository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using IngatlanKozvetito.Data;

    /// <summary>
    /// Repository.
    /// </summary>
    /// <typeparam name="T">T type.</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private static IngatlanDBEntities dataBase = new IngatlanDBEntities();

        /// <inheritdoc/>
        public void Create(T tartalom)
        {
            dataBase.Set<T>().Add(tartalom);
            dataBase.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(string id)
        {
            string idName = typeof(T).GetProperties().Select(x => x.Name).ToList().First(y => y.Contains("id"));
            PropertyInfo typeId = typeof(T).GetProperty(idName);
            var parameter = Expression.Parameter(typeof(T), "x");
            var left = Expression.PropertyOrField(parameter, idName);
            var right = Expression.Constant(Convert.ChangeType(id, typeId.PropertyType), left.Type);
            var condition = Expression.Equal(left, right);
            var predicate = Expression.Lambda<Func<T, bool>>(condition, parameter);
            var result = dataBase.Set<T>().Single(predicate);
            dataBase.Set<T>().Remove(result);
            dataBase.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<T> Read()
        {
                return dataBase.Set<T>().Select(x => x);
        }

        /// <inheritdoc/>
        public void Update(string id, string attributeName, string newValue)
        {
            string idName = typeof(T).GetProperties().Select(x => x.Name).ToList().First(y => y.Contains("id"));
            PropertyInfo typeId = typeof(T).GetProperty(idName);
            PropertyInfo typeAttribute = typeof(T).GetProperty(attributeName);
            var parameter = Expression.Parameter(typeof(T), "x");
            var left = Expression.PropertyOrField(parameter, idName);
            var right = Expression.Constant(Convert.ChangeType(id, typeId.PropertyType), left.Type);
            var condition = Expression.Equal(left, right);
            var predicate = Expression.Lambda<Func<T, bool>>(condition, parameter);
            var item = dataBase.Set<T>().Single(predicate);
            object finalValue = Convert.ChangeType(newValue, typeAttribute.PropertyType);
            item.GetType().GetProperty(attributeName).SetValue(item, finalValue);
            dataBase.SaveChanges();
        }
    }
}
