﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace IngatlanKozvetito.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using IngatlanKozvetito.Data;
    using IngatlanKozvetito.Logic;
    using IngatlanKozvetito.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// This class implements unit tests.
    /// </summary>
    [TestFixture]
    public class Test
    {
        private Mock<IRepository<menedzserek>> moqMenedzserek;
        private Mock<IRepository<ingatlanok>> moqIngatlanok;
        private Mock<IRepository<tipusok>> moqTipusok;
        private Mock<IRepository<berlesek>> moqBerlesek;
        private Mock<IRepository<ugyfelek>> moqUgyfelek;
        private BusinessLogic testLogic;

        /// <summary>
        /// This method is setting up the moq repositories.
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.moqMenedzserek = new Mock<IRepository<menedzserek>>();
            this.moqIngatlanok = new Mock<IRepository<ingatlanok>>();
            this.moqTipusok = new Mock<IRepository<tipusok>>();
            this.moqBerlesek = new Mock<IRepository<berlesek>>();
            this.moqUgyfelek = new Mock<IRepository<ugyfelek>>();
            this.testLogic = new BusinessLogic(this.moqMenedzserek.Object, this.moqBerlesek.Object, this.moqTipusok.Object, this.moqIngatlanok.Object, this.moqUgyfelek.Object);
        }

        /// <summary>
        /// Testing CreateMenedzser method.
        /// </summary>
        [Test]
        public void TestCreateMenedzser()
        {
            menedzserek uj = new menedzserek();
            this.moqMenedzserek.Setup(x => x.Create(uj)).Verifiable();
            this.testLogic.CreateMenedzser(uj);
            this.moqMenedzserek.Verify(x => x.Create(uj), Times.Once);
        }

        /// <summary>
        /// Testing CreateIngatlan method.
        /// </summary>
        [Test]
        public void TestCreateIngatlan()
        {
            ingatlanok uj = new ingatlanok();
            this.moqIngatlanok.Setup(x => x.Create(uj)).Verifiable();
            this.testLogic.CreateIngatlan(uj);
            this.moqIngatlanok.Verify(x => x.Create(uj), Times.Once);
        }

        /// <summary>
        /// Testing CreateBerles method.
        /// </summary>
        [Test]
        public void TestCreateBerles()
        {
            berlesek uj = new berlesek();
            this.moqBerlesek.Setup(x => x.Create(uj)).Verifiable();
            this.testLogic.CreateBerles(uj);
            this.moqBerlesek.Verify(x => x.Create(uj), Times.Once);
        }

        /// <summary>
        /// Testing CreateTipus method.
        /// </summary>
        [Test]
        public void TestCreateTipus()
        {
            tipusok uj = new tipusok();
            this.moqTipusok.Setup(x => x.Create(uj)).Verifiable();
            this.testLogic.CreateTipus(uj);
            this.moqTipusok.Verify(x => x.Create(uj), Times.Once);
        }

        /// <summary>
        /// Testing CreateUgyfel method.
        /// </summary>
        [Test]
        public void TestCreateUgyfel()
        {
            ugyfelek uj = new ugyfelek();
            this.moqUgyfelek.Setup(x => x.Create(uj)).Verifiable();
            this.testLogic.CreateUgyfel(uj);
            this.moqUgyfelek.Verify(x => x.Create(uj), Times.Once);
        }

        /// <summary>
        /// Testing ReadMenedzser method.
        /// </summary>
        [Test]
        public void TestReadMenedzser()
        {
            menedzserek testMenedzser = new menedzserek();
            List<menedzserek> list = new List<menedzserek> { testMenedzser };
            this.moqMenedzserek.Setup(x => x.Read()).Returns(this.TestReturn(list));
            var eredmeny = this.testLogic.ReadMenedzser();
            var varhatoEredmeny = this.TestReturn(list);
            Assert.That(eredmeny, Is.EqualTo(varhatoEredmeny));
        }

        /// <summary>
        /// Testing ReadIngatlan method.
        /// </summary>
        [Test]
        public void TestReadIngatlan()
        {
            ingatlanok testIngatlan = new ingatlanok();
            List<ingatlanok> list = new List<ingatlanok> { testIngatlan };
            this.moqIngatlanok.Setup(x => x.Read()).Returns(this.TestReturn(list));
            var eredmeny = this.testLogic.ReadIngatlan();
            var varhatoEredmeny = this.TestReturn(list);
            Assert.That(eredmeny, Is.EqualTo(varhatoEredmeny));
        }

        /// <summary>
        /// Testing ReadBerles method.
        /// </summary>
        [Test]
        public void TestReadBerles()
        {
            berlesek testBerles = new berlesek();
            List<berlesek> list = new List<berlesek> { testBerles };
            this.moqBerlesek.Setup(x => x.Read()).Returns(this.TestReturn(list));
            var eredmeny = this.testLogic.ReadBerles();
            var varhatoEredmeny = this.TestReturn(list);
            Assert.That(eredmeny, Is.EqualTo(varhatoEredmeny));
        }

        /// <summary>
        /// Testing ReadTipus method.
        /// </summary>
        [Test]
        public void TestReadTipus()
        {
            tipusok testTipus = new tipusok();
            List<tipusok> list = new List<tipusok> { testTipus };
            this.moqTipusok.Setup(x => x.Read()).Returns(this.TestReturn(list));
            var eredmeny = this.testLogic.ReadTipus();
            var varhatoEredmeny = this.TestReturn(list);
            Assert.That(eredmeny, Is.EqualTo(varhatoEredmeny));
        }

        /// <summary>
        /// Testing ReadUgyfel method.
        /// </summary>
        [Test]
        public void TestReadUgyfel()
        {
            ugyfelek testUgyfel = new ugyfelek();
            List<ugyfelek> list = new List<ugyfelek> { testUgyfel };
            this.moqUgyfelek.Setup(x => x.Read()).Returns(this.TestReturn(list));
            var eredmeny = this.testLogic.ReadUgyfel();
            var varhatoEredmeny = this.TestReturn(list);
            Assert.That(eredmeny, Is.EqualTo(varhatoEredmeny));
        }

        /// <summary>
        /// Testing UpdateMenedzser method.
        /// </summary>
        [Test]
        public void TestUpdateMenedzser()
        {
            menedzserek testMenedzser = new menedzserek();
            this.moqMenedzserek.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.testLogic.UpdateMenedzser(testMenedzser.man_id.ToString(), nameof(testMenedzser.nev), "Kígyó Károly");
            this.moqMenedzserek.Verify(x => x.Update(testMenedzser.man_id.ToString(), nameof(testMenedzser.nev), "Kígyó Károly"), Times.Once);
        }

        /// <summary>
        /// Testing UpdateIngatlan method.
        /// </summary>
        [Test]
        public void TestUpdateIngatlan()
        {
            ingatlanok testIngatlan = new ingatlanok();
            this.moqIngatlanok.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.testLogic.UpdateIngatlan(testIngatlan.ingatlan_id.ToString(), nameof(testIngatlan.ker), "22");
            this.moqIngatlanok.Verify(x => x.Update(testIngatlan.ingatlan_id.ToString(), nameof(testIngatlan.ker), "22"), Times.Once);
        }

        /// <summary>
        /// Testing UpdateBerles method.
        /// </summary>
        [Test]
        public void TestUpdateBerles()
        {
            berlesek testBerles = new berlesek();
            this.moqBerlesek.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.testLogic.UpdateBerles(testBerles.ber_id.ToString(), nameof(testBerles.lejarat), "2030.10.10");
            this.moqBerlesek.Verify(x => x.Update(testBerles.ber_id.ToString(), nameof(testBerles.lejarat), "2030.10.10"), Times.Once);
        }

        /// <summary>
        /// Testing UpdateTipus method.
        /// </summary>
        [Test]
        public void TestUpdateTipus()
        {
            tipusok testTipus = new tipusok();
            this.moqTipusok.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.testLogic.UpdateTipus(testTipus.tipus_id.ToString(), nameof(testTipus.tipus_nev), "ujtipus");
            this.moqTipusok.Verify(x => x.Update(testTipus.tipus_id.ToString(), nameof(testTipus.tipus_nev), "ujtipus"), Times.Once);
        }

        /// <summary>
        /// Testing UpdateUgyfel method.
        /// </summary>
        [Test]
        public void TestUpdateUgyfel()
        {
            ugyfelek testUgyfel = new ugyfelek();
            this.moqUgyfelek.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.testLogic.UpdateUgyfel(testUgyfel.berlo_id.ToString(), nameof(testUgyfel.email), "test@test");
            this.moqUgyfelek.Verify(x => x.Update(testUgyfel.berlo_id.ToString(), nameof(testUgyfel.email), "test@test"), Times.Once);
        }

        /// <summary>
        /// Testing DeleteMenedzser method.
        /// </summary>
        [Test]
        public void TestDeleteMenedzser()
        {
            menedzserek testMenedzser = new menedzserek();
            this.moqMenedzserek.Setup(x => x.Delete(It.IsAny<string>())).Verifiable();
            this.testLogic.DeleteMenedzser(testMenedzser.man_id.ToString());
        }

        /// <summary>
        /// Testing DeleteIngatlan method.
        /// </summary>
        [Test]
        public void TestDeleteIngatlan()
        {
            ingatlanok testIngatlan = new ingatlanok();
            this.moqIngatlanok.Setup(x => x.Delete(It.IsAny<string>())).Verifiable();
            this.testLogic.DeleteIngatlan(testIngatlan.ingatlan_id.ToString());
        }

        /// <summary>
        /// Testing DeleteBerles method.
        /// </summary>
        [Test]
        public void TestDeleteBerles()
        {
            berlesek testBerles = new berlesek();
            this.moqBerlesek.Setup(x => x.Delete(It.IsAny<string>())).Verifiable();
            this.testLogic.DeleteBerles(testBerles.ber_id.ToString());
        }

        /// <summary>
        /// Testing DeleteTipus method.
        /// </summary>
        [Test]
        public void TestDeleteTipus()
        {
            tipusok testTipus = new tipusok();
            this.moqTipusok.Setup(x => x.Delete(It.IsAny<string>())).Verifiable();
            this.testLogic.DeleteTipus(testTipus.tipus_id.ToString());
        }

        /// <summary>
        /// Testing DeleteUgyfel method.
        /// </summary>
        [Test]
        public void TestDeleteUgyfel()
        {
            ugyfelek testUgyfel = new ugyfelek();
            this.moqUgyfelek.Setup(x => x.Delete(It.IsAny<string>())).Verifiable();
            this.testLogic.DeleteUgyfel(testUgyfel.berlo_id.ToString());
        }

        /// <summary>
        /// Testing AtlagIngatlanAr method.
        /// </summary>
        [Test]
        public void TestAtlagIngatlanAr()
        {
            List<tipusok> testTipusok = new List<tipusok>();
            testTipusok.Add(new tipusok() { tipus_id = 30, tipus_nev = "teszt" });
            List<ingatlanok> testIngatlanok = new List<ingatlanok>();
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 1, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 100, negyzetmeter = 1 });
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 2, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 300, negyzetmeter = 1 });
            this.moqIngatlanok.Setup(x => x.Read()).Returns(testIngatlanok.AsQueryable);
            this.moqTipusok.Setup(x => x.Read()).Returns(testTipusok.AsQueryable);
            var result = this.testLogic.AtlagIngatlanar();
            Assert.That(result.First().Atlagar, Is.EqualTo(200));
        }

        /// <summary>
        /// Testing HaromLegolcsobbKereses method.
        /// </summary>
        [Test]
        public void TestHaromLegolcsobbKereses()
        {
            List<tipusok> testTipusok = new List<tipusok>();
            testTipusok.Add(new tipusok() { tipus_id = 30, tipus_nev = "teszt", lakhato = 1 });
            List<ingatlanok> testIngatlanok = new List<ingatlanok>();
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 1, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 100, negyzetmeter = 1 });
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 2, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 200, negyzetmeter = 1 });
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 3, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 300, negyzetmeter = 1 });
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 4, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 400, negyzetmeter = 1 });
            this.moqIngatlanok.Setup(x => x.Read()).Returns(testIngatlanok.AsQueryable);
            this.moqTipusok.Setup(x => x.Read()).Returns(testTipusok.AsQueryable);
            var result = this.testLogic.HaromLegolcsobbKereses(1);
            Assert.That(result.Sum(x => x.ingatlan_id), Is.EqualTo(6));
        }

        /// <summary>
        /// Testing HaromLegolcsobbKereses method.
        /// </summary>
        [Test]
        public void TestUgyfelekBerletiSzerint()
        {
            List<ugyfelek> testUgyfelek = new List<ugyfelek>();
            testUgyfelek.Add(new ugyfelek() { nev = "Tesztelo1", berlo_id = 1 });
            testUgyfelek.Add(new ugyfelek() { nev = "Tesztelo2", berlo_id = 2 });
            testUgyfelek.Add(new ugyfelek() { nev = "Tesztelo3", berlo_id = 3 });
            List<ingatlanok> testIngatlanok = new List<ingatlanok>();
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 1, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 100, negyzetmeter = 1 });
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 2, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 200, negyzetmeter = 1 });
            testIngatlanok.Add(new ingatlanok() { tipus_id = 30, ingatlan_id = 3, man_id = 1, ker = 1, butorozott = 0, berleti_dij = 300, negyzetmeter = 1 });
            List<berlesek> testBerlesek = new List<berlesek>();
            testBerlesek.Add(new berlesek() { berlo_id = 1, ber_id = 1, ingatlan_id = 1 });
            testBerlesek.Add(new berlesek() { berlo_id = 2, ber_id = 2, ingatlan_id = 2 });
            testBerlesek.Add(new berlesek() { berlo_id = 3, ber_id = 3, ingatlan_id = 3 });
            this.moqUgyfelek.Setup(x => x.Read()).Returns(testUgyfelek.AsQueryable);
            this.moqIngatlanok.Setup(x => x.Read()).Returns(testIngatlanok.AsQueryable);
            this.moqBerlesek.Setup(x => x.Read()).Returns(testBerlesek.AsQueryable);
            var result = this.testLogic.UgyfelekBerletiSzerint();
            Assert.That(result.First().Contains("Tesztelo3"));
        }

        private IQueryable<T> TestReturn<T>(List<T> list)
        {
            return list.AsQueryable();
        }
    }
}
