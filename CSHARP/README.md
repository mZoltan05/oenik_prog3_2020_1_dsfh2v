IngatlanKozvetito

Funkciók:
	- Ingatlan típusok listázása / törlése / módosítása / új hozzáadása
	- Elérhető ingatlanok listázása / törlése / módosítása / új hozzáadása
	- Menedzserek listázása / törlése / módosítása / új hozzáadása
	- Bérlések listázása / törlése / módosítása / új hozzáadása
	- Ügyfelek listázása / törlése / módosítása / új hozzáadása
	- Típus szerinti átlagos ingatlanár kiírása
	- Legolcsóbb 3 lakható ingatlan kiírása az adott kerületben
	- Ügyfelek listázása az általuk fizetett bérleti díj szerint
	